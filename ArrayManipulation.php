<pre>
<?php

function Test1()
{
    $arr = array("element1" => array("foo" => "bar", "bla" => "blub"));
    foreach ($arr as $element)
    {
        unset($element["foo"]);
        print_r($element);
    }
    print_r($arr);
}

echo PHP_EOL;

function Test2()
{
    $arr = array("element1" => array("foo" => "bar", "bla" => "blub"));
    foreach ($arr as &$element)
    {
        unset($element["foo"]);
        print_r($element);
    }
    print_r($arr);
}

Test1();
Test2();

?>
</pre>